<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Infrastructure\Repositories;

/**
 * Base Repository : Manage the controller communication with the 
 * database layer ( models )
 *
 * @author eday
 */
class BaseRepository implements BaseRepositoryInterface{
    
    
    protected $errorMessage = null;
    protected $model;
    protected $withParams = [];
    public $paginate = true;
    protected $limit;
    protected $where = [
        'get' => [],
        'all' => []
    ];
    protected $currentQuery = null;
    protected $rules = [];
    protected $validator;
    protected $currentObject;
    protected $selectFields = ['*'];
    protected $orderByParams = [
        ['id', 'DESC']
    ];
    protected $linked = [];
    
    /**
     * Get request to return all data ( paginated by default ) after 
     * applying the filters in the request
     * 
     * @param Request $request
     * @return Collection
     */
    public function all($request) {
        $requestData = $request->all();
        $limit = $request->get("limit", -1);
        return $this->getData($requestData, $limit);
    }
    
    protected function getData($requestData, $limit)
    {
        $this->startQuery();
        $data = [];
        if(!empty($requestData))
        {
            if(isset($requestData['filters']))
            {
                $filters = $requestData['filters'];
                $filersFields = explode(',', $filters);
                foreach ($filersFields as $eachFilter) {
                    $filterData = explode(':', $eachFilter);
                    $nestedLoop = explode('.', $filterData[0]);
                    if(count($nestedLoop)>1)
                    {

                    }else{

                        if(count($filterData) == 3) 
                        {
                            $this->where([$filterData[0], $filterData[1], $filterData[2]]);
                        }elseif(count($filterData) == 2)
                        {
                            $this->where([$filterData[0], "=", $filterData[1]]);
                        }
                    }
                }
            }
            
            if(isset($requestData['sort_by']))
            {
                $sort = $requestData['sort_by'];
                $sortData = explode(',', $sort);
                if(!empty($sortData))
                {
                    $this->orderByParams = [];
                    foreach ($sortData as $eachSortObj) {
                        $sortObj = explode(":", $eachSortObj);
                        $type = 'ASC';
                        $field = $sortObj[0];
                        if(count($sortObj)>1)
                        {
                            $type = $sortObj[1];
                        }
                        $this->orderByParams[] = [$field, $type];
                    }
                }
            }
            
        }
        
        if($limit == -1)
        {
            if($this->paginate)
            {
                $data = $this->prepareQuery()->paginate($this->limit);
            }else{
                $data = $this->prepareQuery()->get();
            }
        }else{
            $data = $this->prepareQuery()->take($limit)->get();
        }
        
        return $data;
        
    }

    /**
     * Delete record
     * @param type $id
     * @return boolean
     */
    public function destroy($id) {
        $obj = $this->model->find($id);
        if(empty($obj))
            return false;
        $obj->delete();
        return true;
    }

    /**
     * Get Database record 
     * @param type $id
     * @return Laravel Eloquent Object
     */
    public function get($id) {
        return $this->startQuery()->prepareQuery()->where("id", "=", $id)->first();
    }

    /**
     * Return storedObject after save and update request
     * @return Laravel Eloquent Object
     */
    public function getStoredObject() {
        return $this->storedObject;
    }

    
    /**
     * Apply the where and order by statements in the repository 
     * and get the query Object
     * @return type
     */
    public function prepareQuery() {
//        $tableName = $this->model->getTable();
        if($this->currentQuery == null)
        {
            $this->startQuery();
        }

        foreach ($this->orderByParams as $each) {
            $this->currentQuery
                    ->orderBy($each[0], $each[1]);
        }
//        ->orderBy($this->orderByParams[0], $this->orderByParams[1])
        foreach($this->where['get'] as $key => $value)
        {
            $this->currentQuery->where($value[0], $value[1], $value[2]);
        }

        return $this->currentQuery;
    }


    /**
     * Start new query with the select and with variables
     * @return $this
     */
    public function startQuery() {
        $this->currentQuery = $this->model->select($this->selectFields)
           ->with($this->withParams);
           return $this;
    }

    /**
     * Store data in to the database 
     * @param type $data
     * @return boolean
     */
    public function store($data) {
        $tableName = $this->model->getTable();
        if(key_exists('password', $data))
        {
            $data['password'] = \Hash::make($data['password']);
        }
        
        /**
         * Add nested items to the rules array 
         */
        foreach($this->linked as $key => $value)
        {
            if(key_exists($key, $data))
            {
                unset($this->rules[$key."_id"]);
                $this->rules = \App\Infrastructure\Services\Utils\ArrayToDot::merge($this->rules, $value['mode']::$rules, $key);
            }
        }
        
        /**
         * Validate the input user input  
         */
        if(!$this->validateInput($data))
            return false;
        
        /*
         * Store nested items
         */
        foreach ($this->linked as $key => $value) {
            $tempRepository = new $value['repo'](new $value['model']);
            $tempRepository->store($data[$key]);
            unset($data[$key]);
            $data[$key."_id"] = $tempRepository->getStoredObject()->id;
        }
        
        /**
         * Encode if array or object to JSON
         */
        foreach($data as $key => $value)
        {
            if(is_object($value) || is_array($value))
            {
                $data[$key] = json_encode($value);
            }
        }
        
        $data = $this->saveUserCredentials($data, $tableName);
        $this->storedObject = new $this->model($data);
        $this->storedObject->save();
        return true;
    }
    
    
    /**
     * Set user credentials if user related fields are available
     * 
     * @param $data
     * @param $tableName
     * @return type
     */
    public function saveUserCredentials($data, $tableName)
    {
        if(\Schema::hasColumn($tableName, 'user_id') && \Auth::check()){
               $data['created_by']  = \Auth::id();
        }
        
        if(\Schema::hasColumn($tableName, 'created_by') && \Auth::check()){
               $data['created_by']  = \Auth::id();
        }

        if(\Schema::hasColumn($tableName, 'company_id') && \Auth::check())
        {
            $data['company_id'] = \Auth::user()->company_id;
        }
        return $data;
    }

    /**
     * Update database record
     * 
     * @param type $id
     * @param type $data
     * @return boolean
     */
    public function update($id, $data) {
        $tableName = $this->model->getTable();
        if(key_exists('password', $data))
            $data['password'] = \Hash::make($data['password']);
        $this->storedObject = $this->model->find($id);
        
        /**
         * Add nested items to the rules array 
         */
        foreach($this->linked as $key => $value)
        {
            if(key_exists($key, $data))
            {
                unset($this->rules[$key."_id"]);
                $this->rules = \App\Infrastructure\Services\Utils\ArrayToDot::merge($this->rules, $value['mode']::$rules, $key);
            }
        }
        
        if(empty($this->linked))
        {
            $this->storedObject = $this->model->find($id);
            $rules = [];
            foreach ($data as $key => $value) {

                if(is_object($value) || is_array($value))
                {
                    $data[$key] = json_encode($value);
                }else{
                    $data[$key] = strip_tags($value, '<div><p><a><br><span>');
                }

                $this->storedObject->$key = $data[$key];
                if(array_key_exists($key, $this->rules))
                {
                    $rules[$key] = $this->rules[$key];
                }
            }
            $this->rules = $rules;
        }
        
        $this->validator = \Validator::make($data, $this->rules);
        if($this->validator->fails())
        {
            return false;
        }
        
        foreach ($this->linked as $key => $value) {
            if(isset($data[$key])){
                $tempRepository = new $value['repo'](new $value['model']);
                if(isset($data[$key]['id'])){
                    $tempRepository->update($data[$key]['id'], $data[$key]);
                }else{
                    $tempRepository->store( $data[$key]);
                    $data[$key."_id"] = $tempRepository->getStoredObject()->id;
                }
                unset($data[$key]);
            }
        }
        
        foreach ($data as $key => $value) {

            if(is_object($value) || is_array($value))
            {
                $data[$key] = json_encode($value);
            }else{
                $data[$key] = strip_tags($value, '<div><p><a><br><span>');
            }

            if(\Schema::hasColumn($tableName, $key) &&
                    $this->storedObject->$key != $data[$key]){
                $this->storedObject->$key = $data[$key];
            }
            if(array_key_exists($key, $this->rules))
            {
                $rules[$key] = $this->rules[$key];
            }
        }
        $this->storedObject->save();



        $this->storedObject = $this->get($this->storedObject->id);
        return true;
        
    }

    public function validateInput($data) {
        $this->validator = \Validator::make($data, $this->rules);
        if($this->validator->fails())
        {
            return false;
        }
        return true;
    }

    public function where($data, $type = "get") {
        if(array_key_exists($type, $this->where))
        {
            $this->where[$type][] = $data;
        }else{
            $this->where[$type] = [$data];
        }
        return $this;
    }

    
    public function model($model)
    {
        $this->model = $model;
        return $this;
    }

    public function rules($rules){
        $this->rules  = $rules;
        return $this;
    }

    public function fields($fields)
    {
        $this->selectFields = $fields;
        return $this;
    }

    public function getErrors()
    {
        return (isset($this->validator)?$this->validator->messages():[]);
    }

    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    public function with($params){
        $this->withParams = $params;
        return $this;
    }

    public function paginate($state = true, $limit=10){
        $this->paginate = $state;
        $this->limit = $limit;
        return $this;
    }
}
