<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Infrastructure\Repositories;

/**
 *
 * @author eday
 */
interface BaseRepositoryInterface {
    public function all($request);
    public function get($id);
    public function store($data);
    public function update($id, $data);
    public function destroy($id);
    
    public function model($model);
    public function rules($rules);
    public function fields($fields);
    public function getErrors();
    public function getErrorMessage();
    public function with($params);
    public function where($data, $type="get");
    public function validateInput($data);
    public function getStoredObject();
    
    public function startQuery();
    public function prepareQuery();
    
    public function paginate($state = true, $limit = 10);
}
