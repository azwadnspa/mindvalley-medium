<?php
namespace App\Repositories;

class ArticleRepository extends \App\Infrastructure\Repositories\BaseRepository{

    public function __construct(\App\Models\Article $model) {
        $this->model = $model;
        $this->rules = \App\Models\File::$rules;
        $this->with([]);
    }
    

}
