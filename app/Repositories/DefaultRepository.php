<?php
namespace App\Repositories;
use Illuminate\Http\Request;

class DefaultRepository extends \App\Infrastructure\Repositories\BaseRepository
{

    protected $resource =[];
    public function _model($model)
    {
        try{
            $this->model = \App::make("\\App\\Models\\".
                    ucfirst(camel_case(str_singular($model)))
                    );
            $class= "\\App\\Models\\".
                    ucfirst(camel_case(str_singular($model)));
            $this->rules = $class::$rules;
            $this->resource = \Config::get("resources.".$model);
//            if(!is_null($this->resource))
//            {
//                $this->with($resources['with']);
//            }
        }catch(\Exception $e){
            dd("Model Not Found.");
        }

    }
    
    public function store($data) {
        $check = parent::store($data);
        if($check)
        {
            $tableName = $this->model->getTable();
            if(\Schema::hasColumn($tableName, 'subsidy_account_id')){
                $this->storeAuditLog($this->getStoredObject()->subsidy_account_id, 
                    $this->getStoredObject()->id, 
                    'sa-default-'.$tableName,
                    'create',
                    $this->getStoredObject(), 
                    $tableName);
            }
        }
        return $check;
    }
    
    public function update($id, $data, $validationOption = 1) {
        $check = parent::update($id, $data, $validationOption);
        if($check)
        {
            $tableName = $this->model->getTable();
            if(\Schema::hasColumn($tableName, 'subsidy_account_id')){
                $this->storeAuditLog($this->getStoredObject()->subsidy_account_id, 
                    $this->getStoredObject()->id, 
                    'sa-default-'.$tableName,
                    'update',
                    $this->getStoredObject(), 
                    $tableName);
            }
        }
        return $check;
    }

    public function getAll($request) {
        if(!is_null($this->resource))
        {
            $this->with($this->resource['with']['all']);
        }
        return parent::getAll($request);
    }

    public function get($id) {
        if(!is_null($this->resource))
        {
            $this->with($this->resource['with']['get']);
        }
        return parent::get($id);
    }

}
