<?php
namespace App\Repositories;

class FileRepository extends \App\Infrastructure\Repositories\BaseRepository{

    public function __construct(\App\Models\File $model) {
        $this->model = $model;
        $this->rules = \App\Models\File::$rules;
        $this->with([]);
    }
    

}
