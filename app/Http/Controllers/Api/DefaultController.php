<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;

class DefaultController extends \App\Infrastructure\Controllers\ResponseController
{
    public $repository;

    public function __construct() {
        $this->repository  = \App::make('\App\Repositories\DefaultRepository');
    }

    public function create($urlAttr) {
        $this->repository->_model($urlAttr);
    }

    public function destroy($urlAttr, $id) {
        $this->repository->_model($urlAttr);
        if($this->repository->destroy($id))
        {
            return $this->success("Succcessfully Deleted", []);
        }
        return $this->error("Unable to find the required record.");
    }

    public function edit($urlAttr, $id) {
        $this->repository->_model($urlAttr);

    }

    public function index($urlAttr, Request $request) {
        $this->repository->_model($urlAttr);
        $this->repository->paginate = $request->get("paginate", $this->repository->paginate);
        $paginationState = $this->repository->paginate;
        $data = $this->repository->all($request);
        if($paginationState)
        {
            $responseArray = $data->toArray();
            $responseArray['code'] = 200;
            $responseArray['message'] = "Data";
            return response($responseArray, 200);
        }

        if(empty($data))
        {
            return $this->success("No Data Found", []);
        }
        return $this->success("Date Returned", $data->toArray());
    }

    public function show($urlAttr, $id) {
        $this->repository->_model($urlAttr);
        $data = $this->repository->get($id);

        if(empty($data) || empty($data->toArray()))
        {
            return $this->error("No Record Found.");
        }
        return $this->success("Record Retrieved.", $data);
    }

    public function store($urlAttr, Request $request) {
        $this->repository->_model($urlAttr);
        $data = $request->all();
        if(!$this->repository->store($data))
        {
            return $this->response(405, "Validation Errors", $this->repository->getErrors()->toArray(), 422);
        }
        return $this->success("Saved Successfully.", $this->repository->getStoredObject());
    }

    public function update($urlAttr, Request $request,$id) {
        $this->repository->_model($urlAttr);
        $data = $request->all();
        if(!$this->repository->update($id, $data))
        {
            return $this->response(405, "Validation Errors", $this->repository->getErrors(), 422 );
        }
        return $this->success('Updated Successfully.', $this->repository->getStoredObject());
    }

    public function error($message) {
        return $this->response(400, $message, [], 400);
    }

    public function response($statusCode, $message, $data = [], $httpCode = 200) {
        return response(['code' => $statusCode, 'message' => $message, 'data'=>$data], $httpCode);
    }

    public function success($message, $data) {
        return $this->response(200, $message, $data, 200);
    }

}
