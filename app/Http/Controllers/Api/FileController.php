<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;

class FileController extends \App\Infrastructure\Controllers\ResponseController
{
    /**
     *
     * @var \App\Repositories\FileRepository
     */
    public $repo;
    
    public function __construct(\App\Repositories\FileRepository $repo) {
        $this->repo = $repo;
    }
    
    public function previewFile($id)
    {
        $file = \App\Models\File::find($id);
        if(!empty($file))
        {
            return response()->file(storage_path().
                    DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'files'
                    .DIRECTORY_SEPARATOR.$file->file_name);
        }
        return 'File Not Found.';
    }
    
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'file' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->response(405, "Validation Errors", $validator->errors()->all(), 422);
        }
//        $file = $request->file("file");
        $extension = $request->file->extension();
        $fileName = date("YmdHis").'.'.$extension;
        $path = $request->file->storeAs('files', $fileName);
        
        $data = [
            'file_name' => $fileName,
            'file_type' => $extension,
            'path' => $path
        ];
        $fileObj = \App\Models\File::create($data);
        $responseData = [
            'file' => $fileObj,
            'configKey' => $request->get('config-queue')
        ];
        return $this->success('File Obj', $responseData);
    }
}