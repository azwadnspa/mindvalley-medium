<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticleController extends Controller
{
    
    public $articleRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(\App\Repositories\ArticleRepository $articleRepo)
    {
        $this->articleRepository = $articleRepo;
//        $this->middleware('auth');
    }
    
    public function getArticleView($slug)
    {
        $data = $this->articleRepository->where(['slug', '=', $slug])->prepareQuery()->first();
        return $this->loadView('pages.article', $data, []);
    }
    
    public function getArticlesView()
    {
        $data = $this->articleRepository->where(['status', '=', 'published'])
                ->prepareQuery()->get();
        return $this->loadView('pages.articles', $data, []);
    }
    
    public function getFrontPageView()
    {
        $data = $this->articleRepository->where(['status', '=', 'published'])
                ->prepareQuery()->get();
        return $this->loadView('pages.articles', $data, []);
    }
    
    
    public function loadView($page, $data, $config = [])
    {
        $config['social-links'] = [
            'Facebook' => 'http://facebook.com',
            'twitter' => 'http://twitter.com'
        ];
        if(!isset($config['page']))
        {
            $config['page'] = [
                'title' => 'Mind Valley Blog',
                'about_us' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vel mi ut felis tempus commodo nec id erat. Suspendisse consectetur dapibus velit ut lacinia.'
            ];
        }
        return view($page, ['data' => $data, 'config' => $config]);
    }
    

    
}
