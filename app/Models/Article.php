<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public static $rules = [
        'title' => 'required', 
        'content' => '', 
        'image' => 'required',
        'tags' => ''
    ];

    protected $fillable = [
        'title', 'status', 'content', 'tags', 'created_by','image', 'slug'
    ];
    
    
    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->slug = str_slug($model->title);
        });
    }
    
    public function getContentAttribute($data)
    {
        return json_decode($data);
    }
    
    public function getTagsAttribute($data)
    {
        return json_decode($data);
    }
    
    public function getImageAttribute($data)
    {
        return json_decode($data);
    }
}
