<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleLog extends Model
{
    public static $rules = [
        'article_id' => 'required',
        'status' => '', 
        'title' => '', 
        'content' => '',
        'tags' => ''
    ];

    protected $fillable = [
        'title', 'status', 'content', 'tags', 'created_by', 'article_id'
    ];
    
    public function getContentAttribute($data)
    {
        return json_decode($data);
    }
    
    public function getTagsAttribute($data)
    {
        return json_decode($data);
    }
    
}
