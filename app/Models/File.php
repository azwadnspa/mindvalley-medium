<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public static $rules = [
        'file_name' => 'required',
        'is_active',
        'path',
        'file_type' => 'required'
    ];

    protected $fillable = [
        'file_name', 'path', 'is_active',
        'file_type','created_by'
    ];
}
