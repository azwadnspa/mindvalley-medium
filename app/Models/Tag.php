<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public static $rules = [
        'name' => 'required', 
        'description' => '', 
    ];

    protected $fillable = [
        'name', 'description', 'created_by'
    ];
    
}
