<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public static $rules = [
        'article_id' => 'required', 
        'subject' => 'required', 
        'description' => ''
    ];

    protected $fillable = [
        'article_id', 'subject', 'description', 'created_by'
    ];
    
    
}
