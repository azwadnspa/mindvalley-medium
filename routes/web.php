<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ArticleController@getFrontPageView');

Route::get('/article/{slug}', 'ArticleController@getArticleView' );
Route::get('/articles', 'ArticleController@getArticlesView' );
Route::get('/home', 'ArticleController@getArticlesView')->name('home');


Route::group([ 'middleware' => ["web", 'auth']], function()
{
    Route::get('/admin', function () {
            return view('admin');
        });
});

/**
 * APU ROUTES
 */
Route::group(['prefix' => 'v1', 'middleware' => ["web", 'auth']], function()
{
    Route::get("/api/{model}", 'Api\DefaultController@index');
    Route::get("/api/{model}/{id}", 'Api\DefaultController@show');
    Route::delete("/api/{model}/{id}", 'Api\DefaultController@destroy');
    Route::post('/api/{model}', 'Api\DefaultController@store');
    Route::put('/api/{model}/{id}', 'Api\DefaultController@update');
    
});

//Auth::routes();
Route::get('/login',  'Auth\LoginController@showLoginForm')->name("login");
Route::post('/login',  'Auth\LoginController@login')->name("login");
Route::get('/logout', 'Auth\LoginController@logout');

// FILE UPLOAD
Route::post("/upload/file", 'Api\FileController@store');
Route::get("/preview/file/{id}", 'Api\FileController@previewFile');

