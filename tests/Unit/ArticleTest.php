<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ArticleTest extends TestCase
{
    
    
    /**
     * Basic crud testing
     */
    
     public function testCreateArticleValidationError()
    {
        $user = \App\User::find(1);

        $response = $this->actingAs($user)->withHeaders([
            'X-Header' => 'Value',
        ])->json('POST', '/v1/api/articles', ['title' => '', 'image' => ['id' => 1]]);

        $response
            ->assertStatus(422); 
    }
    
    public function testCreateArticle()
    {
        $user = \App\User::find(1);

        $response = $this->actingAs($user)->withHeaders([
            'X-Header' => 'Value',
        ])->json('POST', '/v1/api/articles', ['title' => 'Article', 'image' => ['id' => 1]]);

        $response
            ->assertStatus(200); 
    }
    
    public function testUpdateArticle()
    {
        $user = \App\User::find(1);

        $response = $this->actingAs($user)->withHeaders([
            'X-Header' => 'Value',
        ])->json('PUT', '/v1/api/articles/1', ['title' => 'Article', 'image' => ['id' => 1]]);

        $response
            ->assertStatus(200); 
    }
    
    public function testGetArticle()
    {
        $user = \App\User::find(1);

        $response = $this->actingAs($user)->json('GET', '/v1/api/articles/1');

        $response
            ->assertStatus(200); 
    }
    
    public function testGetArticleWithoutUser()
    {
        $response = $this->json('GET', '/v1/api/articles/1');

        $response
            ->assertStatus(401); 
    }
}
