
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Vuex from 'vuex'
import VueRouter from 'vue-router'

import Toasted from 'vue-toasted';

Vue.use(VueRouter);
Vue.use(Vuex);

Vue.use(Toasted, {position: 'bottom-center' });
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('nc-component', require('./components/NewsComponents/NewsCardComponent.vue').default);
//Vue.component('nl-component', require('./components/NewsComponents/NewsListComponent.vue').default);



/**
 * Vue Components
 */
import NewListComponent from './components/admin/NewsListController.vue';
import TComponent from './components/admin/NewsController.vue';


/**
 * Routes
 */
const routes = [
    {path : '/article-manage/:id', name: 'article-manage', component: TComponent},
    {path : '/', name: 'home-view', component: NewListComponent},
];

const router = new VueRouter({routes});

const app = new Vue({
    router
}).$mount('#app');
