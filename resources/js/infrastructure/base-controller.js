import mymix from  './base-model.js'
import { store } from './../components/stores/formStore'

/**
 * Base Controller consistent with Laravel routes
 */
export default {
  extends  : mymix,
  store,
  data : function()
  {
    return {
         alert : {
            state : false,
            message: ''
        },
      items : [],
      showModal : false,
      formMode :  'new',
      formConfigurations : [],
      form : {},
      formErrors : [],
      relationshipObjs : [],
      filterObj : {
          type : '',
          value : ''
      },
      isLoading : false,
      isFormLoading : false
    };
  },
  methods : {
      showAlert : function(message)
      {
          this.alert.state = true;
          this.alert.message = message;
          setTimeout(this.hideAlert, 2000);
            
      },
      hideAlert : function()
      {
          this.alert.state = false;
      },
      filterAll : function()
      {
          this.pageVar  = 1;
          this.clearFilters().setFilters(this.filterObj.type+":"+this.filterObj.value);
          this.loadItems();
      },
      clearAndFilter : function()
      {
          this.pageVar  = 1;
          this.filterObj = {
                type : '',
                value : ''
          };
          this.clearFilters().loadItems();
      },
      loadData : function()
      {
          this.pageVar = this.pagination.current_page;
          this.loadItems();
      },
    loadItems : function()
    {
      var self = this;
      self.isLoading = true;
      this.all().then(function(){
          self.items = self.response.data;
          self.isLoading = false;
      });
    },
    showAddForm : function(){
      this.formMode = 'new';
      this.form = {};
      this.$store.commit('change', this.form);
      this.showModal = true;
    },
    showEditFormByObject : function(formObj)
    {
      this.formMode = 'edit';
      this.form = this.copy(formObj);
      var self = this;
      this.relationshipObjs.forEach(function(relObj){
          if(typeof self.form[relObj] !== "undefined")
          {
              delete self.form[relObj];
          }
      });

      console.log(this.form);
      this.showModal = true;
    },
    showEditForm : function(index)
    {
      this.formMode = 'edit';
      this.form = this.copy(this.items[index]);
      this.$store.commit('change', this.form);
      var self = this;
      this.relationshipObjs.forEach(function(relObj){
          if(typeof self.form[relObj] !== "undefined")
          {
              delete self.form[relObj];
          }
      });
      console.log(this.relationshipObjs);
      console.log(this.form);
      this.showModal = true;
    },
    saveForm : function()
    {
      console.log(this.form);
      var self = this;
      self.isFormLoading = true;
      if(this.formMode === 'new'){
        this.store(this.form).then(function(){
            self.isFormLoading = false;
            if(self.errorResponse === null){
                self.all().then(function(){
                    self.showModal = false;
                    self.items = self.response.data;
                });
            }
        });
      }else{
        this.update(this.form.id, this.form).then(function(response){
            self.isFormLoading = false;
            if(self.errorResponse === null){
                self.all().then(function(){
                    self.showModal = false;
                    self.items = self.response.data;
                });
                
            }
        });
      }
    },
    deleteRecord : function(index)
    {
        var self = this;
        this.remove(this.items[index].id)
        .then(function(response){
            console.log(response);
            self.items.splice(index, 1);
        });
    }
  }
}
