export default {
        'create' : {
            form : [
                {
                    label : 'Title',
                    type : 'input:text',
                    formModel : 'title'
                },
                {
                    label : 'File',
                    type : 'file',
                    formModel : 'image'
                }
            ]
        },
        'image' : {
            form: [
                {
                    label : 'File',
                    type : 'file',
                    formModel : 'image'
                }
            ]
        }
};