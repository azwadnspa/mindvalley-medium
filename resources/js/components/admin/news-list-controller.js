import baseCtrl from  './../../infrastructure/base-controller.js'
//import BreadCrumb from './../utils/BreadCrumb.vue'
import Dialog from './../utils/Dialog.vue'
import PaginateComponent from './../utils/PaginateComponent.vue'
import FormGen from './../utils/form-gen/FormGen.vue'
//import { store } from './../stores/formStore'
//import FormConfig from './sub-form-configs/form.config'
import articlesConfig from './form-config/articles'

export default {
        extends : baseCtrl,
        components : {  
            'ed-dialog' : Dialog,
            'ed-pagination' : PaginateComponent,
            'form-gen' :  FormGen
        },
        mounted() {
            this.init();
        },
        data() {
            return {
                filterConfigs : [],
                viewRouteResourceName : 'default-view',
                resourceName : 'articles'
            };
        },
        methods : {
            init()
            {
                this.paginate = true;
                this.formConfigurations = articlesConfig['create'].form;
                this.loadItems();
            }
        }
}