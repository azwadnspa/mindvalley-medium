import baseCtrl from  './../../infrastructure/base-controller.js'
//import BreadCrumb from './../utils/BreadCrumb.vue'
import Dialog from './../utils/Dialog.vue'
import PaginateComponent from './../utils/PaginateComponent.vue'
import FormGen from './../utils/form-gen/FormGen.vue'
import { store } from './../stores/formStore'
//import FormConfig from './sub-form-configs/form.config'
import editor from 'vue2-medium-editor'
import articlesConfig from './form-config/articles'
import VueTagsInput from '@johmun/vue-tags-input';

export default {
        extends : baseCtrl,
        store,
        components : {  
            'ed-dialog' : Dialog,
            'ed-pagination' : PaginateComponent,
            'form-gen' :  FormGen,
            'medium-editor': editor,
            'vue-tags-input' : VueTagsInput
        },
        mounted() {
            this.isLoading = true;
            this.init(this.$route);
        },
        beforeRouteUpdate(to, from, next)
        {
            this.init(to);
            next();
        },
        data() {
            return {
                articleID: null,
                myText: '',
                mediumOptions: {
                    toolbar: {buttons: ['bold', 'strikethrough', 'h1', 'h2', 'anchor']}
                  },
                filterConfigs : [],
                viewRouteResourceName : 'default-view',
                resourceName : 'articles',
                articleConfigs : [
                    {content: '', type: 'text'}
                ],
                dialogConfig : {
                    title : 'UploadImage',
                    formConfiguration : null,
                    func : function(){
                        
                    }
                },
                tag: '',
                tags: [],
                autocompleteItems: [],
                debounce: null,
            };
        },
        watch: {
            'tag': 'initItems',
          },
        methods : {
            init(routeVar)
            {
                var self = this;
                this.articleID = routeVar.params.id;
                this.get(this.articleID).then(()=>{
                    self.isLoading = false;
                    if(self.selectedItem.content){
                        self.articleConfigs = self.selectedItem.content;
                    }
                    if(self.selectedItem.tags){
                        self.tags = self.selectedItem.tags;
                    }
                });
            },
            updateTags(newTags) {
                console.log('tags', newTags);
                this.autocompleteItems = [];
                this.tags = newTags;
              },
              initItems() {
                if (this.tag.length < 2) return;
                const url = `/v1/api/tags?filters=name:LIKE:${this.tag}%`;
          
                clearTimeout(this.debounce);
                this.debounce = setTimeout(() => {
                  axios.get(url).then(response => {
                    this.autocompleteItems = response.data.data.map(a => {
                      return { text: a.name };
                    });
                  }).catch(() => console.warn('Oh. Something went wrong'));
                }, 600);
              },
            dialogSave: function()
            {
                this.dialogConfig.func();
            },
            saveArticle: function()
            {
                var self = this;
                this.selectedItem.tags  = this.tags;
                this.selectedItem.content = this.articleConfigs;
                this.update(this.selectedItem.id, this.selectedItem).then(()=>{
                    if(self.errorResponse === null){
                        Vue.toasted.success('Saved Successfully');
                    }else{
                        Vue.toasted.error('Error Updating');
                    }
                });

            },

            updateStatus : function(status)
            {
                this.selectedItem.status = status;
                this.selectedItem.tags  = this.tags;
                
                this.selectedItem.content = this.articleConfigs;
                this.update(this.selectedItem.id, this.selectedItem).then(()=>{
                    
                        Vue.toasted.success('Saved Successfully');
                   
                });
            },

            editMainImage : function()
            {
                var self = this;
                this.form = {};
                this.$store.commit('change', this.form);
                this.dialogConfig = {
                    title : 'UploadImage',
                    formConfiguration : articlesConfig['image'].form,
                    func : function(){
                        self.selectedItem.image =  store.state.form.image;
                        self.showModal = false;
                    }
                };
                this.showModal = true;
            },
            editImageContent : function(index)
            {
                var self = this;
                this.form = {};
                this.$store.commit('change', this.form);
                this.dialogConfig = {
                    title : 'UploadImage',
                    formConfiguration : articlesConfig['image'].form,
                    func : function(){
                        self.articleConfigs[index] = {content: store.state.form.image, type: 'image'};
                        self.showModal = false;
                    }
                };
                this.showModal = true;
            },
            addImageToContent : function()
            {
                var self = this;
                this.form = {};
                this.$store.commit('change', this.form);
                this.dialogConfig = {
                    title : 'UploadImage',
                    formConfiguration : articlesConfig['image'].form,
                    func : function(){
                        self.articleConfigs.push({content: store.state.form.image, type: 'image'});
                        self.showModal = false;
                    }
                };
                this.showModal = true;
            },
            addContentBlock: function()
            {
                console.log(this.articleConfigs);
                this.articleConfigs.push({content: '', type: 'text'});
            },
            removeArticleContent : function(index)
            {
                this.articleConfigs.splice(index, 1);
            },
            applyTextEdit: function (operation) {
                var o = operation.api.origElements.attributes[0].nodeValue;
                this.articleConfigs[o ].content = operation.api.origElements.innerHTML;
            }
        }
}