import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    form: {}
  },
  mutations: {
    change(state, form) {
      state.form = form;
      console.log('START');
      console.log(state.form);
      console.log('END');

    }
  },
  getters: {
    form: state => state.form
  }
});