[
    {
        label: 'Address',
        type: 'input:text',
        formModel: 'address'
    },
    {
        label: "Select",
        type: 'select',
        formModel: 'selectName',
        selectOptions: {
            option: 'id',
            value: 'name'
        },
        dataType: 'api',
        dataApi: '/api/companies',
        dataArray: []
    },
    {
        label: "Select",
        type: 'select',
        formModel: 'selectoption',
        selectOptions: {
            option: 'id',
            value: 'title'
        },
        dataType: 'array',
        dataArray: [
            {
                id: 1,
                title: 'TESTER'
            }
        ]
    }
]