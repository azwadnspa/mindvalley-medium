@extends('layouts.app-public')

@section('content')
<!--Container-->
	<div class="container w-full md:max-w-lg mx-auto pt-20">
		
		<div class="w-full px-4 md:px-6 text-xl text-grey-darkest leading-normal" style="font-family:Georgia,serif;">
			
			<!--Title-->
			<div class="font-sans">
				<span class="text-base md:text-sm text-teal font-bold">&lt;<span> 
                                        <a href="/" class="text-base md:text-sm text-teal font-bold no-underline hover:underline">BACK TO BLOG</a></p>
				
                                         <img class="object-cover w-full py-2" 
                                     src='/preview/file/{{$data->image->id}}' >
			
                                <h1 class="font-sans break-normal text-black pb-2 text-3xl md:text-4xl">
                                    {{$data->title}}
                                </h1>
				<p class="text-sm md:text-base font-normal text-grey-dark">Last Updated on {{$data->updated_at}}</p>
			</div>
                        
                        
			
			<!--Post Content-->
			

                        @foreach($data->content as $contentItem)
                            @if($contentItem->type == 'text')
                                <p class="py-2">{!!$contentItem->content!!}</p>
                            @elseif($contentItem->type == 'image')
                                <img class="object-cover w-full py-2" 
                                     src='/preview/file/{{$contentItem->content->id}}' >
                            @endif
                        @endforeach

			

											
			<!--/ Post Content-->
					
		</div>
		
		<!--Tags -->
		<div class="text-base md:text-sm text-grey px-4 py-6">
			Tags: 
                        @foreach($data->tags as $tag)
                            <a href="#" class="text-base md:text-sm text-teal no-underline hover:underline">{{$tag->text}}</a> .  
                        @endforeach
                </div>
		
		
		
		
		
	</div> 
	<!--/container-->
@endsection
