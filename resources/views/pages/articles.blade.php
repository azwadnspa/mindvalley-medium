@extends('layouts.app-public')

@section('content')

	<!--Container-->
        <div class="container w-full md:max-w-lg mx-auto pt-20">

            <div class="w-full px-4 md:px-6 text-xl text-grey-darkest leading-normal" style="font-family:Georgia,serif;">

                <!-- Two columns -->
                <div class="flex mb-4">
                    <div class="w-full px-2">
                        <div class="rounded overflow-hidden shadow-lg">
                            <img class="w-full" src="/preview/file/{{$data[0]->image->id}}" >
                            <div class="px-6 py-4">
                                <div class="font-bold text-xl mb-2">{{$data[0]->title}}</div>
                                <div class="text-grey-darker text-base" style='font-size:80%'>
                                    {!! strlen($data[0]->content[0]->content) > 200 ? substr($data[0]->content[0]->content,0,200)."..." : $data[0]->content[0]->content !!}
                                </div>
                            </div>
                            <div class='float-right'>
                                <a href='/article/{{$data[0]->slug}}'
                                  class="no-underline mr-5 bg-transparent hover:bg-blue text-blue-dark font-semibold hover:text-white py-2 px-4 border border-blue hover:border-transparent rounded"
                                  >View</a>
                            </div>
                            <div class="px-6 py-4">
                                @foreach($data[0]->tags as $tag)
                                    <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker mr-2">#{{$tag->text}}</span>
                                @endforeach
                                
                            </div>
                        </div>
                    </div>
                </div>
                @foreach($data as $key => $obj)
                       @if($key > 0)
                    <div class="flex mb-4">

                                <div class="w-full px-2">
                                    <div class=" rounded overflow-hidden shadow-lg">
                                        <div class='flex'>
                                            <div class='w-1/3'>
                                                
                                                <img class="w-full h-full object-cover" src="/preview/file/{{$obj->image->id}}" >
                                            </div>
                                            <div class='w-2/3'>
                                                <div class="px-6 py-4">
                                                    <div class="font-bold text-xl mb-2">{{$obj->title}}</div>

                                                </div>
                                                <div class='float-right'>
                                                 <a href='/article/{{$obj->slug}}'
                                                        class="no-underline mr-5 bg-transparent hover:bg-blue text-blue-dark font-semibold hover:text-white py-2 px-4 border border-blue hover:border-transparent rounded"
                                                        >View</a>
                                                  </div>
                                                  <div class="px-6 py-4">
                                                      @foreach($obj->tags as $tag)
                                                          <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker mr-2">#{{$tag->text}}</span>
                                                      @endforeach

                                                  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                    </div>
                       @endif
                @endforeach
            </div>





            



        </div> 
        <!--/container-->


        
        
@endsection
