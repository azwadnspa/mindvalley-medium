<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <title>{{config('app.name')}}</title>
        <meta name="author" content="name">
        <meta name="description" content="description here">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/app-tw.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.22.1/css/medium-editor.css">
    </head>
    <body class="bg-grey-lightest font-sans leading-normal tracking-normal">
        <div  id='app'>
            <nav id="header" class="fixed w-full z-10 pin-t">

                <div id="progress" class="h-1 z-20 pin-t" style="background:linear-gradient(to right, #4dc0b5 var(--scroll), transparent 0);"></div>

                <div class="w-full md:max-w-lg mx-auto flex flex-wrap items-center justify-between mt-0 py-3">

                    <div class="pl-4">
                        <a class="text-black text-base no-underline hover:no-underline font-extrabold text-xl"  href="#"> 
                            {{config('app.name')}}
                        </a>
                    </div>

                    <div class="block lg:hidden pr-4">
                        <button id="nav-toggle" class="flex items-center px-3 py-2 border rounded text-grey border-grey-dark hover:text-black hover:border-teal appearance-none focus:outline-none">
                            <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
                        </button>
                    </div>

                    <div class="w-full flex-grow lg:flex lg:items-center lg:w-auto hidden lg:block mt-2 lg:mt-0 bg-grey-lightest md:bg-transparent z-20" id="nav-content">
                        <ul class="list-reset lg:flex justify-end flex-1 items-center">
                            <li class="mr-3">
                                <a class="inline-block py-2 px-4 text-black  no-underline" href="/">Home</a>
                            </li>
                            <li class="mr-3">
                                @guest
                                <a class="inline-block text-grey-dark no-underline hover:text-black hover:text-underline py-2 px-4" 
                                   href="{{ route('login') }}">{{ __('Login') }}</a>
                                @else

                            <router-link to="/" class="inline-block text-grey-dark font-bold no-underline hover:text-black hover:text-underline py-2 px-4">
                                Publications <span class="caret"></span>
                            </router-link>
                            <a href='/logout' class="inline-block text-grey-dark no-underline hover:text-black hover:text-underline py-2 px-4" >
                                 Logout
                            </a>
                            @endguest
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <!--Container-->
            <div class="container w-full md:max-w-lg mx-auto pt-20">

                <div class="w-full px-4 md:px -6 text-xl text-grey-darkest leading-normal" style="font-family:Georgia,serif;">

                    @yield('content')



                </div>

            </div> 
            <!--/container-->
        </div>

        <script>
            /* Progress bar */
            //Source: https://alligator.io/js/progress-bar-javascript-css-variables/
            var h = document.documentElement,
                    b = document.body,
                    st = 'scrollTop',
                    sh = 'scrollHeight',
                    progress = document.querySelector('#progress'),
                    scroll;
            var scrollpos = window.scrollY;
            var header = document.getElementById("header");
            var navcontent = document.getElementById("nav-content");

            document.addEventListener('scroll', function () {

                /*Refresh scroll % width*/
                scroll = (h[st] || b[st]) / ((h[sh] || b[sh]) - h.clientHeight) * 100;
                progress.style.setProperty('--scroll', scroll + '%');

                /*Apply classes for slide in bar*/
                scrollpos = window.scrollY;

                if (scrollpos > 10) {
                    header.classList.add("bg-white");
                    header.classList.add("shadow");
                    navcontent.classList.remove("bg-grey-lightest");
                    navcontent.classList.add("bg-white");
                } else {
                    header.classList.remove("bg-white");
                    header.classList.remove("shadow");
                    navcontent.classList.remove("bg-white");
                    navcontent.classList.add("bg-grey-lightest");

                }

            });


            //Javascript to toggle the menu
            document.getElementById('nav-toggle').onclick = function () {
                document.getElementById("nav-content").classList.toggle("hidden");
            }


        </script>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>