@extends('layouts.appv1')

@section('content')

<div class="w-full max-w-xs login-form mr-auto ml-auto">
  <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" 
        method="POST" action="{{ route('login') }}">
                        @csrf
    <div class="mb-4">
      <label class="block text-grey-darker text-sm font-bold mb-2" for="username">
        Username
      </label>
      <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" 
                 class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus 
                 placeholder="Email">
      @if ($errors->has('email'))
            <span class="text-red text-xs italic">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    <div class="mb-6">
      <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
        Password
      </label>
      <input class="shadow appearance-none border  rounded w-full py-2 px-3 text-grey-darker mb-3 leading-tight focus:outline-none focus:shadow-outline" id="password" 
             type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="******************">
        @if ($errors->has('password'))
            <span class="text-red text-xs italic">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
    <div class="flex items-center justify-between">
      <button class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" 
              type="submit">
        {{ __('Login') }}
      </button>
    
    </div>
  </form>

</div>


@endsection
