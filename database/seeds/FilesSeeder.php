<?php

use Illuminate\Database\Seeder;

class FilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\File::create([
            'file_name' => '20190424175142.jpeg',
            'file_type' => 'jpeg'
        ]);
        \App\Models\File::create([
            'file_name' => '20190424175142.jpeg',
            'file_type' => 'jpeg'
        ]);
        
        File::copy(base_path('seed-images'.DIRECTORY_SEPARATOR.'20190424175142.jpeg'),
                base_path('storage'.DIRECTORY_SEPARATOR.
                        'app'.DIRECTORY_SEPARATOR.
                        'files'.DIRECTORY_SEPARATOR.'20190424175142.jpeg'));
        File::copy(base_path('seed-images'.DIRECTORY_SEPARATOR.'20190424175142.jpeg'),
                base_path('storage'.DIRECTORY_SEPARATOR.
                        'app'.DIRECTORY_SEPARATOR.
                        'files'.DIRECTORY_SEPARATOR.'20190424175142.jpeg'));
    }
}
