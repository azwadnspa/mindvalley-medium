<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Article::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(6),
        'image' => json_encode(['id' => 1]),
        'content' => json_encode([['content' => $faker->paragraph(), 'type' => 'text'], ['content' => $faker->paragraph(), 'type' => 'text']] ),
        'tags' => json_encode([['text' => $faker->word]]),
        'status'  => 'published',
        'created_by' => 1
    ];
});
