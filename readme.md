## Simple Medium Blog 

Simple blog to manage blog posts similar to Medium. Developed using Laravel, VueJS, tailwindcss.


## Installation

Laravel 5.7 
composer install 
npm install or use yarn 

Run migrations
php artisan migrate:seed


Database :
Postgresql 10

Testing: 
Basic CRUD functions tested.
./vendor/bin/phpunit



